package com.example.data.repository

import com.example.data.database.models.ImageSchema
import com.example.data.database.models.ImageSchema.Companion.toImageModel
import com.example.data.database.models.ImageSchema.Companion.toRealmSchema
import com.example.data.database.safeRealmQuery
import com.example.data.network.GiphyImageService
import com.example.data.network.safeApiCall
import com.example.domain.models.Image
import com.example.domain.models.Result
import com.example.domain.repository.ImageRepository
import io.realm.kotlin.Realm
import io.realm.kotlin.ext.query
import javax.inject.Inject

/**
 * Repository
 *
 * Репозиторий для получения и хранения изображений
 */
class ImageRepositoryImpl @Inject constructor(
    private val imageService: GiphyImageService,
    private val database: Realm
) : ImageRepository {
    override suspend fun getImageFromApi(): Result<Image> {

        return when (val responseResult = imageService.getImages().safeApiCall()) {
            is Result.Error -> responseResult
            is Result.Failure -> responseResult
            is Result.Success -> {
                val image = responseResult.data.data.firstOrNull()

                image?.let { Result.Success(image.images.downsized) }
                    ?: Result.Failure("empty list")
            }
        }
    }

    override suspend fun addImageToFavorites(image: Image): Result<Boolean> {
        return safeRealmQuery {
            database.write {
                copyToRealm(image.toRealmSchema())
            }
            true
        }
    }

    override suspend fun getFavoriteImages(): Result<List<Image>> {
        //TODO Reactive
        return safeRealmQuery {
            database.query(ImageSchema::class).find().map { it.toImageModel() }
        }
    }

    override suspend fun deleteFavoriteImage(image: Image): Result<Boolean> {
        return safeRealmQuery {
            database.write {
                val img = this.query<ImageSchema>("url == $0", image.url)
                delete(img)
            }
            true
        }
    }
}