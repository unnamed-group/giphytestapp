package com.example.data.database.models

import com.example.domain.models.Image
import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.annotations.PrimaryKey

/**
 * Модель изображения для Realm DB
 */
open class ImageSchema(
    @PrimaryKey var url: String = "",
    var pathToFile: String = ""
) : RealmObject {

    constructor() : this(url = "", pathToFile = "")

    companion object {
        fun Image.toRealmSchema(): ImageSchema {
            return ImageSchema(url = this.url, pathToFile = this.pathToFile)
        }

        fun ImageSchema.toImageModel(): Image {
            return Image(url = this.url, pathToFile = this.pathToFile)
        }
    }
}