package com.example.data.database

import android.content.Context
import com.example.data.database.models.ImageSchema
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import io.realm.kotlin.Realm
import io.realm.kotlin.RealmConfiguration
import javax.inject.Singleton

/**
 * Синглтон Realm DB
 */
@InstallIn(SingletonComponent::class)
@Module
object RealmModule {
    @Provides
    @Singleton
    fun providesRealmDatabase(@ApplicationContext context: Context): Realm {
        return Realm.open(getRealmConfiguration())
    }

    private fun getRealmConfiguration(): RealmConfiguration {
        return RealmConfiguration
            .create(schema = setOf(ImageSchema::class))
    }

    private const val REALM_NAME = "GiphyRealm"
}