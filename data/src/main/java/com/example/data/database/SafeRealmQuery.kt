package com.example.data.database

import com.example.domain.models.Result
import java.lang.IllegalArgumentException

/**
 * Обертка для запросов к Realm DB
 */
suspend fun <T> safeRealmQuery(block: suspend () -> T): Result<T> {
    return try {
        Result.Success(block.invoke())
    } catch (exception: IllegalArgumentException) {
        Result.Error(exception)
    } catch (exception: IllegalStateException) {
        Result.Error(exception)
    }
}