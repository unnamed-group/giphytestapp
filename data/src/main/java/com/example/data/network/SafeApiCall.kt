package com.example.data.network

import retrofit2.Response
import com.example.domain.models.Result

/**
 * Обертка для запросов Retrofit
 */
fun <T> Response<T>.safeApiCall(): Result<T> {
    return try {
        if (this.isSuccessful && this.body() != null) {
            Result.Success(this.body()!!)
        } else Result.Failure(this.message())
    } catch (exception: Exception) {
        Result.Error(exception)
    }
}
