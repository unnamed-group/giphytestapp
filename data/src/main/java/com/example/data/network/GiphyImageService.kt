package com.example.data.network

import com.example.domain.models.GetImagesResponseBody
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Интерфейс сервиса изображений Giphy для Retrofit
 */
interface GiphyImageService {

    @GET("gifs/trending")
    suspend fun getImages(
        @Query("api_key") apiKey: String = API_KEY,
        @Query("limit") limit: Int = 1
    ): Response<GetImagesResponseBody>

    companion object {
        //Giphy API KEY
        private const val API_KEY = "Whc2AX8NBhg0Wynxc6O8xWOy4wg51tsP"
    }
}