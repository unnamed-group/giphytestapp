# GiphyTestApp
Android 5.0+

Реализовано
- Работа с сетью (Вывод GIF изображений с API)
- Работа с БД (CRUD изображений)
- SplashScreen

В процессе:
- Работа в оффлайне
- Оповещение о новых изображениях

Используемые технологии:
- Clean Architecture
- Kotlin
- Coroutines
- Hilt dependency injection
- Realm database
- Retrofit
- Jetpack navigation
- Glide