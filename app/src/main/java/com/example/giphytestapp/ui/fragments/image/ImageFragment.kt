package com.example.giphytestapp.ui.fragments.image

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.bumptech.glide.Glide
import com.example.giphytestapp.databinding.FragmentIconBinding
import dagger.hilt.android.AndroidEntryPoint

/**
 * Фрагмент актуального изображения
 */
@AndroidEntryPoint
class ImageFragment : Fragment() {
    private lateinit var binding: FragmentIconBinding
    private val viewModel by viewModels<ImageViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentIconBinding.inflate(inflater, container, false)

        viewModel.imageLiveData.observe(viewLifecycleOwner) { image ->
            showImage(image.url)
        }

        binding.addToFavoriteButton.setOnClickListener {
            viewModel.addImageToFavorite()
        }

        return binding.root
    }

    private fun showImage(url: String) {
        Glide.with(this)
            .asGif()
            .load(url)
            .into(binding.imageView)
    }
}