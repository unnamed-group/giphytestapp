package com.example.giphytestapp.ui.fragments.favorites.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.domain.models.Image
import com.example.giphytestapp.databinding.ItemFavoriteImageBinding

/**
 * Адаптер списка избранных изображений
 */
class FavoritesGridAdapter(
    private val onDeleteClick: (Image) -> Unit
) : RecyclerView.Adapter<FavoritesGridAdapter.ViewHolder>() {

    private val images = ArrayList<Image>()

    fun setImages(images: List<Image>) {
        this.images.apply {
            clear()
            addAll(images)
        }
    }

    class ViewHolder(val binding: ItemFavoriteImageBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemFavoriteImageBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount() = images.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = images[position]

        holder.binding.apply {
            Glide.with(root.context)
                .asGif()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .load(item.url)
                .into(itemImage)

            itemDeleteButton.setOnClickListener {
                onDeleteClick(item)
            }
        }
    }
}