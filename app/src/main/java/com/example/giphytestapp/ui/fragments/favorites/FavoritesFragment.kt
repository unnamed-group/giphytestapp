package com.example.giphytestapp.ui.fragments.favorites

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.example.giphytestapp.databinding.FragmentFavoritesBinding
import com.example.giphytestapp.ui.fragments.favorites.adapter.FavoritesGridAdapter
import com.example.giphytestapp.util.GridItemDecorator
import dagger.hilt.android.AndroidEntryPoint

/**
 * Фрагмент списка избранных изображений
 */
@AndroidEntryPoint
class FavoritesFragment : Fragment() {
    private lateinit var binding: FragmentFavoritesBinding
    private val viewModel by viewModels<FavoritesViewModel>()
    private val favoriteImagesAdapter = FavoritesGridAdapter { imageForDelete ->
        viewModel.deleteImage(imageForDelete)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentFavoritesBinding.inflate(inflater, container, false)

        binding.imagesRecyclerView.apply {
            layoutManager = GridLayoutManager(context, RECYCLER_SPAN_COUNT)
            adapter = favoriteImagesAdapter
            addItemDecoration(GridItemDecorator(GRID_SPACING, RECYCLER_SPAN_COUNT))
        }

        viewModel.favoriteImagesLiveData.observe(viewLifecycleOwner) { images ->
            favoriteImagesAdapter.setImages(images)
        }

        return binding.root
    }

    companion object {
        private const val RECYCLER_SPAN_COUNT = 2
        private const val GRID_SPACING = 16
    }
}