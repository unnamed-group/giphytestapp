package com.example.giphytestapp.ui.fragments.image

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.models.Image
import com.example.domain.models.Result
import com.example.domain.usecase.AddImageToFavoriteUseCase
import com.example.domain.usecase.GetImageUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ImageViewModel @Inject constructor(
    private val getImageUseCase: GetImageUseCase,
    private val addImageToFavoriteUseCase: AddImageToFavoriteUseCase
) : ViewModel() {

    init {
        getImage()
    }

    private val _imageLiveData = MutableLiveData<Image>()
    val imageLiveData: LiveData<Image>
        get() = _imageLiveData

    private var currentImage: Image? = null

    private fun getImage() {
        viewModelScope.launch(Dispatchers.IO) {
            when (val result = getImageUseCase.execute()) {
                is Result.Error -> Unit
                is Result.Failure -> Unit
                is Result.Success -> {
                    result.data.let { image ->
                        currentImage = image
                        _imageLiveData.postValue(image)
                    }
                }
            }
        }
    }

    fun addImageToFavorite() {
        currentImage?.let { image ->
            viewModelScope.launch(Dispatchers.IO) {
                when (addImageToFavoriteUseCase.execute(image)) {
                    is Result.Error -> Unit
                    is Result.Failure -> Unit
                    is Result.Success -> Unit
                }
            }
        }
    }
}