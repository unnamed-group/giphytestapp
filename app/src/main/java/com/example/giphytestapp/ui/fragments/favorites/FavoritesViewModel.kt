package com.example.giphytestapp.ui.fragments.favorites

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.models.Image
import com.example.domain.models.Result
import com.example.domain.usecase.DeleteFavoriteImagesUseCase
import com.example.domain.usecase.GetFavoriteImagesUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * ViewModel списка избранных изображений
 */
@HiltViewModel
class FavoritesViewModel @Inject constructor(
    private val getFavoriteImagesUseCase: GetFavoriteImagesUseCase,
    private val deleteFavoriteImageUseCase: DeleteFavoriteImagesUseCase
) : ViewModel() {

    init {
        getFavoriteImages()
    }

    private val _favoriteImagesLiveData = MutableLiveData<List<Image>>()
    val favoriteImagesLiveData: LiveData<List<Image>>
        get() = _favoriteImagesLiveData

    fun deleteImage(image: Image) {
        viewModelScope.launch(Dispatchers.IO) {
            when (deleteFavoriteImageUseCase.execute(image)) {
                is Result.Error -> Unit
                is Result.Failure -> Unit
                is Result.Success -> {
                    getFavoriteImages() //TODO Удалить при реактивном getFavoriteImagesUseCase.execute()
                }
            }
        }
    }

    private fun getFavoriteImages() {
        viewModelScope.launch(Dispatchers.IO) {
            when (val images = getFavoriteImagesUseCase.execute()) {
                is Result.Error -> Unit
                is Result.Failure -> Unit
                is Result.Success -> {
                    _favoriteImagesLiveData.postValue(images.data)
                }
            }
        }
    }
}