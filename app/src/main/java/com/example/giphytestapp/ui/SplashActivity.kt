package com.example.giphytestapp.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.giphytestapp.R
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

/**
 * Реализация SpashScreen
 *
 * Для более нового и стандартизированного SplashScreen Android 12+ смотреть сюда:
 * androidx.core:core-splashscreen
 * https://developer.android.com/develop/ui/views/launch/splash-screen/migrate
 */
@SuppressLint("CustomSplashScreen")
@AndroidEntryPoint
class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        CoroutineScope(Dispatchers.Main).launch {
            delay(SPLASH_DELAY)
            startMainActivity()
        }
    }

    private fun startMainActivity() {
        startActivity(
            Intent(this@SplashActivity, MainActivity::class.java)
        )
    }

    companion object {
        private const val SPLASH_DELAY = 2000L
    }
}