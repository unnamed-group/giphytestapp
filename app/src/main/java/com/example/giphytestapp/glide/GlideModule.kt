package com.example.giphytestapp.glide

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

/**
 * Модуль Glide
 */
@GlideModule
class GlideModule : AppGlideModule()