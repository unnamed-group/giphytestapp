package com.example.giphytestapp.di

import com.example.data.repository.ImageRepositoryImpl
import com.example.domain.repository.ImageRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

/**
 * Модуль зависимостей Hilt
 */
@InstallIn(ViewModelComponent::class)
@Module
abstract class ImageModule {
    //Repository
    @Binds
    abstract fun getImageRepository(repo: ImageRepositoryImpl): ImageRepository
}