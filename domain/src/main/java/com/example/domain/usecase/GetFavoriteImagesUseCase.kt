package com.example.domain.usecase

import com.example.domain.models.Image
import com.example.domain.models.Result
import com.example.domain.repository.ImageRepository
import javax.inject.Inject

/**
 * UseCase
 *
 * Возвращает список избранных изображений
 */
class GetFavoriteImagesUseCase @Inject constructor(private val imageRepo: ImageRepository) {
    suspend fun execute(): Result<List<Image>> {
        return imageRepo.getFavoriteImages()
    }
}