package com.example.domain.usecase

import com.example.domain.models.Image
import com.example.domain.models.Result
import com.example.domain.repository.ImageRepository
import javax.inject.Inject

/**
 * UseCase
 *
 * Возвращает одно самое актуальное изображение
 */
class GetImageUseCase @Inject constructor(private val imageRepo: ImageRepository) {
    suspend fun execute(): Result<Image> {
        return imageRepo.getImageFromApi()
    }
}