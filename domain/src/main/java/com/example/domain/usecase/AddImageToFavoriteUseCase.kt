package com.example.domain.usecase

import com.example.domain.models.Image
import com.example.domain.repository.ImageRepository
import javax.inject.Inject

/**
 * UseCase
 *
 * Добавляет изображение в избранное
 */
class AddImageToFavoriteUseCase @Inject constructor(private val imageRepo: ImageRepository) {
    suspend fun execute(image: Image) = imageRepo.addImageToFavorites(image)
}