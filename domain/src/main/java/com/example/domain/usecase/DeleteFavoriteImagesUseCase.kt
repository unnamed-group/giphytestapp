package com.example.domain.usecase

import com.example.domain.models.Image
import com.example.domain.models.Result
import com.example.domain.repository.ImageRepository
import javax.inject.Inject

/**
 * UseCase
 *
 * Удаляет изобрадение из избранногго
 */
class DeleteFavoriteImagesUseCase @Inject constructor(private val imageRepo: ImageRepository) {
    suspend fun execute(image: Image): Result<Boolean> {
        return imageRepo.deleteFavoriteImage(image)
    }
}