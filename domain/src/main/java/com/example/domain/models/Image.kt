package com.example.domain.models

/**
 * Модель изображения
 */
data class Image(
    val url: String = "",
    val pathToFile: String = ""
)