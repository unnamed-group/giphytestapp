package com.example.domain.models

/**
 * Обертка для данных
 */
sealed class Result<out T> {
    class Success<out T>(val data: T) : Result<T>()
    class Failure(val message: String) : Result<Nothing>()
    class Error(val exception: Exception) : Result<Nothing>()
}