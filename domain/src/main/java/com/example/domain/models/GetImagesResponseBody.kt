package com.example.domain.models

/**
 * Модели для парса Json, полученного через Retrofit
 */
class GetImagesResponseBody {
    val data: List<ImageResponseBody> = emptyList()
}

data class ImageResponseBody(
    val images: Images
)

data class Images(
    val downsized: Image
)