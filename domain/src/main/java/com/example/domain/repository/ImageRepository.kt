package com.example.domain.repository

import com.example.domain.models.Image
import com.example.domain.models.Result

/**
 * Repository
 *
 * Интерфейс репозитория взаимодействия с изображениями
 */
interface ImageRepository {
    suspend fun getImageFromApi(): Result<Image>

    suspend fun addImageToFavorites(image: Image): Result<Boolean>

    suspend fun getFavoriteImages() : Result<List<Image>>

    suspend fun deleteFavoriteImage(image: Image): Result<Boolean>
}